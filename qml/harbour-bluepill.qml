import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0

import "pages"
import "components"

ApplicationWindow
{
    id: bluepill

//    property string homeserver: homeServerUrlConf.value  // not needed in UI
//    property string identityserver: identityServerUrlConf.value // not needed in UI
//    property string token: sessionTokenConf.value // not needed in UI!
//    property string userName: userNameConf.value // just a variable not needed to be stored
//    property string userId: userIdConf.value // same as userId
    property bool engineLoaded: false

    // style stuff should be somewhere else

    property var usercolor: [ "#368bd6", "#ac3ba8", "#03b381", "#e64f7a", "#ff812d", "#2dc2c5", "#5c56f5", "#74d12c"]
    property string htmlcss: '<style>a:link { color: ' + Theme.highlightColor + '; }</style>'

    initialPage: Qt.resolvedUrl("pages/StartPage.qml")

    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations

//    ConfigurationValue { // not needed
//        id: userNameConf
//        key: "/apps/ControlPanel/bluepill/userName"
//        defaultValue: ""
//    }

//    ConfigurationValue { // not needed
//        id: userIdConf
//        key: "/apps/ControlPanel/bluepill/userId"
//    }

//    ConfigurationValue { // not needed
//        id: sessionTokenConf
//        key: "/apps/ControlPanel/bluepill/sessionToken"
//        defaultValue: ""
//    }

//    ConfigurationValue { // not needed
//        id: homeServerUrlConf
//        key: "/apps/ControlPanel/bluepill/homeServerUrl"
//        defaultValue: "https://matrix.org"
//    }

//    ConfigurationValue { // not needed
//        id: identityServerUrlConf
//        key: "/apps/ControlPanel/bluepill/identityServerUrl"
//        defaultValue: "https://vector.im"
//    }

    function hashCode(str) {
        var hash = 0
        var i
        var chr
        if (str.length === 0) {
            return hash
        }
        for (i = 0; i < str.length; i++) {
            chr = str.charCodeAt(i)
            hash = ((hash << 5) - hash) + chr
            hash |= 0
        }
        return Math.abs(hash);
    }

    function u_color(user_id) {
        return usercolor[hashCode(user_id) % 8]
    }

    ClientHandlerPython {
        id: clienthandler

    }
}
