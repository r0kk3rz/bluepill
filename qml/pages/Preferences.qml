import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    id: page
    allowedOrientations: Orientation.All

    SilicaFlickable {
        id: loginflick
        anchors.fill: parent
        contentHeight: loginTitle.height + loginTitle.spacing + parameters.height + parameters.spacing

        Connections {
            target: clienthandler
            onLoggedOut: {
                pageStack.replace(Qt.resolvedUrl("LoginPage.qml"))
            }
        }

        Column {
            id: loginTitle

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Preferences")
            }
        }


        Column {
            id: parameters
            width: parent.width
            height: children.height
            anchors {
                top: loginTitle.bottom
                left: parent.left
                right: parent.right
                margins: Theme.paddingMedium
            }

            Button {
                id: logoutButton
                property bool logging: false
                anchors.margins: Theme.paddingLarge
                anchors.bottomMargin: Theme.paddingMedium
                text: qsTr("Logout")
                BusyIndicator {
                    size: BusyIndicatorSize.Medium
                    anchors.centerIn: logoutButton
                    running: logoutButton.logging
                }

                onClicked: {
                    Remorse.popupAction(page, qsTr("Logging out"), function() {
                        logging = true
                        clienthandler.doLogout()
                    })
                }
            }
        }
    }
}
