import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page
    property bool eLoaded: bluepill.engineLoaded
    // The effective value will be restricted by ApplicationWindow.allowedOrientations

    Connections {
        target: clienthandler
        onSyncingRooms: {
            statusLabel.text = qsTr("Syncing room: " + roomname)
        }
        onInitReady: {
            pageStack.replace(Qt.resolvedUrl("DashboardPage.qml"))
        }
    }

    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        // Tell SilicaFlickable the height of its content.
        contentHeight: parent.height

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.
        Image {
            id: logo
            anchors.centerIn: parent
            source: "../../images/matrix_logo.png"
            fillMode: Image.PreserveAspectFit
            width: Theme.iconSizeExtraLarge * 2
        }
        Label {
            id: statusLabel
            anchors.bottom: parent.bottom
            padding: Theme.paddingMedium
            text: qsTr("Starting engine...")
            font.pixelSize: Theme.fontSizeTiny
            color: Theme.highlightColor
        }
    }
}
