import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

CoverBackground {
    id: coverback
//    Component.onCompleted: {
//        clienthandler.getRooms()
//    }

    property string lastevent: "none"
    property string roomid: ""
    property string roomname: ""

    function getText(event) {
        if (event.type === "m.room.message") {
            if (event.content.format) {
                if (event.content.format === 'org.matrix.custom.html') {
                    return bluepill.htmlcss + event.content.formatted_body
                }
            }
            return event.content.body
        } else if (event.type === "m.sticker") {
            return ""
        } else if (event.type === "m.room.encrypted") {
            return ""
        } else {
            return ""
        }
    }

    Connections {
        target: clienthandler
        onRoomsList: {
            if (data.length > 0) {
                var room = data[0]
                usericon.uid = room.user_id
                usericon.avatarurl = room.avatar_url ? room.avatar_url : ""
                usericon.idirect = room.direct
                usericon.roomname = room.room_name
                usericon.visible = true
                roomid = room
                roomname = room.room_name
            }
        }

        onMRoomEvent: {
            clienthandler.getRoom(roomId)
            console.log(event.event.content.body)
            if (event.event.content.body) {
                roomid = roomId
                roomname = event.name
                coverback.lastevent = "m.room.event"
                coverlabel.text = getText(event.event)
                writericon.uid = event.user_id
                writericon.roomname = event.name
                writericon.avatarurl = event.avatar_url ? event.avatar_url : ""
                writericon.idirect = true
                writericon.visible = true
                writeAnim.stop()
                writerrow.opacity = 1.0
                messageAnim.start()
            }
        }
        onRoomData: {
            usericon.avatarurl = room.avatar_url ? room.avatar_url : ""
            usericon.uid = room.user_id
            usericon.idirect =  room.direct
            usericon.roomname = room.room_name
            usericon.visible = true
            roomid = room_id
            roomname = room.room_name
        }
        onStartTyping: {
            clienthandler.getRoom(roomId)
            writericon.uid = users[0].user_id
            writericon.roomname = users[0].user_name
            writericon.avatarurl = users[0].image
            writericon.visible = true
            coverlabel.text = qsTr("is typing...")
            writeAnim.start()
            coverback.lastevent = "m.typing"
        }
        onStopTyping: {
            if (coverback.lastevent == "m.typing") {
                writeAnim.stop()
                writericon.visible = false
                coverlabel.text = ""
                coverback.lastevent = "was_m.typing"
            }
        }
    }

    Image {
        source: "../../images/matrix_logo.png"
        width: parent.width
        rotation: 315
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        fillMode: Image.PreserveAspectFit
        opacity: 0.1
    }
    Column {
        id: spacer
        height: Theme.iconSizeExtraSmall
        width: parent.width
    }

    Column {
        id: uiconcol
        anchors.top: spacer.bottom
        width: parent.width
        height: (parent.width < parent.height ? parent.width : parent.height) * 0.6
        z: 1
        UserIcon {
            id: usericon
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.height
            height: parent.height
            avatarurl: ""
            roomname: ""
            idirect: false
            uid: ""
            visible: false
            SequentialAnimation {
                id: messageAnim
                running: false
                loops: 2

                NumberAnimation {
                    target: usericon
                    property: "opacity"
                    duration: 1000
                    to: 0.5
                }
                NumberAnimation {
                    target: usericon
                    property: "opacity"
                    duration: 1000
                    to: 1.0
                }
            }
        }
    }
    Row {
        id: writerrow
        anchors.top: uiconcol.bottom
        width: parent.width
        height: Theme.itemSizeLarge
        clip: true
        SequentialAnimation {
            id: writeAnim
            running: false
            loops: 20

            NumberAnimation {
                target: writerrow
                property: "opacity"
                duration: 1000
                to: 0.5
            }
            NumberAnimation {
                target: writerrow
                property: "opacity"
                duration: 1000
                to: 1.0
            }
        }

        UserIcon {
            id: writericon
            width: Theme.iconSizeSmall
            height: Theme.iconSizeSmall
            visible: false
            avatarurl: ""
            roomname: ""
            idirect: false
            uid: ""

        }
        Label {
            id: coverlabel
            // anchors.top: usericon.bottom
            text: ""
            width: parent.width - writericon.width
            height: Theme.itemSizeLarge
            wrapMode: Text.WordWrap
            truncationMode: TruncationMode.Fade
            font.pixelSize: Theme.fontSizeTiny
            textFormat: Text.RichText
            padding: Theme.paddingSmall
        }
   }

    CoverActionList {
        id: coverAction

        CoverAction {
            iconSource: "image://theme/icon-cover-new"
        }

        CoverAction {
            iconSource: "image://theme/icon-cover-message"
            onTriggered: {
                bluepill.activate()
                if (roomid != "") {
                    pageStack.pop(null, true)
                    pageStack.push(Qt.resolvedUrl("../pages/RoomPage.qml"), { room_id: roomid, room_name: roomname } )
                }
            }
        }
    }
}
