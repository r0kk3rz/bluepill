import QtQuick 2.0
import Sailfish.Silica 1.0

Column {
    id: textcontrol
    property string lname: ""
    property string avatar_url: ""
    property string user_id: ""
    property string room_id
    property int typetimeout: 15000
    width: parent.width
    height: inforow.height + textrow.height
    anchors {
        left: parent.left
        right: parent.right
    }

    Row {
        id: inforow
        width: parent.width
        height: Theme.itemSizeMedium
        spacing: Theme.paddingMedium
        Column {
            id: uicol
            spacing: Theme.paddingMedium
            width: Theme.iconSizeMedium
            UserIcon {
                id: infouser
                width: Theme.iconSizeSmall
                height: Theme.iconSizeSmall
                visible: false
            }
        }
        Column {
            id: infocol
            anchors.left: uicol.right
            width: parent.width - uicol.width - ab.width
            height: Theme.itemSizeMedium
            Label {
                id: infobox
                width: parent.width
                height: parent.height
                padding: Theme.paddingMedium
                // anchors.verticalCenter: parent.verticalCenter
                text: ""
                font.pixelSize: Theme.fontSizeSmall
                color: Theme.highlightColor
                wrapMode: Text.WordWrap
                truncationMode: TruncationMode.Fade
            }
        }
        SequentialAnimation {
            id: blinkAnim
            running: false
            loops: Animation.Infinite

            NumberAnimation {
                targets: [infocol, uicol]
                properties: "opacity"
                to: 0.5
                duration: 1000
            }
            NumberAnimation {
                targets: [infocol, uicol]
                properties: "opacity"
                to: 1.0
                duration: 1000
            }
        }
        IconButton {
            id: ab
            anchors.right: parent.right
            icon.source: "image://theme/icon-s-attach"
        }
    }
    Item {
        Connections {
            target: clienthandler
            onMessageSent: {
                console.log("Message sent")
                infouser.visible = false
                infobox.visible = false
                inputfield.text = ""
            }
            onStopTyping: {
                if (room_id !== roomId) {
                    return
                }
                blinkAnim.running =  false
                infouser.visible = false
                infobox.text = ""
                infocol.opacity = 1.0
                uicol.opacity = 1.0
            }
            onStartTyping: {
                console.log(textcontrol.user_id, ",", users[0].user_id)
                if (room_id !== roomId || textcontrol.user_id === users[0].user_id) {
                    return
                }

                console.log("starts typing: ", users[0].user_name)
                blinkAnim.running = true
                infouser.roomname = users[0].user_name
                infouser.avatarurl = users[0].image
                infouser.uid = users[0].user_id
                infouser.visible = true
                infobox.text = users[0].user_name + qsTr(" ist typing...")
                infobox.visible = true
            }
        }

        id: textrow
        width: parent.width
        height: ifield.height
        anchors {
            left: parent.left
            right: parent.right
        }

        Column {
            id: userIcon
            anchors {
                left: parent.left
                margins: Theme.paddingMedium
            }
            UserIcon {
                width: Theme.iconSizeMedium
                height: Theme.iconSizeMedium
                avatarurl: avatar_url
                idirect: true
                roomname: lname
                uid: user_id
            }
        }

        Column {
            id: ifield
            anchors {
                left: userIcon.right
            }
            height: inputfield.height
            width: parent.width - userIcon.width - enterkey.width
            TextArea {
                id: inputfield
                property bool isTyping: false
                property string tmptext
                width: parent.width
                placeholderText: room_name
                placeholderColor: Theme.lightSecondaryColor
                onTextChanged: {
                    if (isTyping == false) {
                        console.log("typing")
                        inputfield.isTyping = true
                        typeTimer.running = true
                        clienthandler.isTyping(room_id, typetimeout)
                    }
                    inputfield.tmptext = inputfield.text
                }
                Timer {
                    id: typeTimer
                    interval: typetimeout
                    repeat: true
                    running: false
                    onTriggered: {
                        console.log("typeTimer")
                        console.log(inputfield.tmptext, inputfield.text)
                        if (inputfield.tmptext === inputfield.text) {
                            console.log("is not typing")
                            inputfield.isTyping = false
                            typeTimer.running = false
                        }
                    }
                }
            }
        }
        Column {
            id: enterkey
            anchors.left: ifield.right
            IconButton {
                id: ib
                icon.source: "image://theme/icon-m-enter-accept"
                onClicked: {
                    if (inputfield.text === "") {
                        return
                    }
                    var intext = inputfield.text
                    inputfield.text = " "
                    infouser.roomname = lname
                    infouser.avatarurl = avatar_url
                    infouser.uid = user_id
                    infouser.visible = true
                    infobox.text = intext
                    infobox.visible = true
                    clienthandler.sendMessage(room_id, intext)
                    inputfield.text = ""
                }
            }
        }

    }
}
