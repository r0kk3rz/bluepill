#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Client handler for Bluepill
"""

import pyotherside
import threading
import sys
import os
import pyotherside

sys.path.append("/usr/share/harbour-bluepill/python")

from bluepill.client import BluepillClientFactory
from bluepill.memory import Memory

def get_hostname():
    """
    Return System hostname
    """

    hostname = os.uname().nodename
    pyotherside.send('hostName', hostname)

def do_init():
    """
    Instanciate client and connect to the homeserver
    """

    BluepillClientFactory().get_client().connect()

def do_login(user, password, hostname, homeserver, identserver):
    """
    Login
    """

    client = BluepillClientFactory().get_client()
    if not client:
        pyothserside.send("No BluepillClient initiated")
    token = client.login(user, password, hostname,
                         homeserver, identserver)

def get_rooms():
    """
    Get list of my rooms
    """

    client = BluepillClientFactory().get_client()

    data = []

    for roominfo in client.get_roomlist():
        data.append(roominfo)

    data.sort(key = lambda r: r["time"], reverse=True)
    pyotherside.send("roomsList", data)

def get_room(room_id):
    """
    Get single room info
    """

    client = BluepillClientFactory().get_client()
    if client:
        rd = client.get_room(room_id)
        if rd:
            pyotherside.send("roomData", rd)

def get_room_events(room_id):
    """
    Get room events from room object
    """

    BluepillClientFactory().get_client().get_room_events(room_id)

def get_room_members(self, room_id):
        """
        get room events for room_id
        """

        rd = Memory().get_object(room_id)
        members = rd.members
        pyotherside.send("roomMembers", room_id,  members)

def get_user_info():
    """
    Get user data of logged in user
    """

    BluepillClientFactory().get_client().get_user_info()

def send_message(room_id, message):
    """
    Send a message
    """

    BluepillClientFactory().get_client().send_message(room_id, message)
    pyotherside.send("messageSent")

def seen_message(room_id):
    """
    Mark seen messages
    """

    BluepillClientFactory().get_client().seen_message(room_id)

def is_typing(room_id, timeout=30000):
    """
    User is typing
    """

    BluepillClientFactory().get_client().is_typing(room_id, timeout)

def do_logout():
    """
    Logout
    """

    BluepillClientFactory().get_client().logout()

class ClientHandler:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()

        self.bgthread1 = threading.Thread()
        self.bgthread1.start()

        self.bgthread2 = threading.Thread()
        self.bgthread2.start()

        pyotherside.atexit(self.doexit)

    def getrooms(self):
        if self.bgthread1.is_alive():
            return
        self.bgthread1 = threading.Thread(target=get_rooms)
        self.bgthread1.start()

    def getroom(self, room_id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_room, args=[room_id])
        self.bgthread.start()

    def getroomevents(self, room_id):
        if self.bgthread2.is_alive():
            return
        self.bgthread2 = threading.Thread(target=get_room_events, args=[room_id])
        self.bgthread2.start()

    def getroommembers(self, room_id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_room_members, args=[room_id])
        self.bgthread.start()


    def startroomlistener(self, room_id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=start_room_listener, args=[room_id])
        self.bgthread.start()

    def stoproomlistener(self, room_id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=stop_room_listener, args=[room_id])
        self.bgthread.start()

    def seenmessage(self, room_id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=seen_message, args=[room_id])
        self.bgthread.start()

    def istyping(self, room_id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=is_typing, args=[room_id])
        self.bgthread.start()

    def getuserinfo(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_user_info)
        self.bgthread.start()

    def dologout(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=do_logout)
        self.bgthread.start()

    def doexit(self):
        """
        Exit the system on leaving app
        """

        BluepillClientFactory().get_client().do_exit()

clienthandler = ClientHandler()
