import QtQuick 2.0
import Sailfish.Silica 1.0
import QtGraphicalEffects 1.0

Column {
    id: roomItem
    width: Theme.itemSizeExtraLarge
    height: 100


    UserIcon {
        id: itemrec
        anchors.horizontalCenter: parent.horizontalCenter
        avatarurl: avatar_url ? avatar_url : ""
        idirect: direct
        roomname: room_name
        uid: user_id
        width: Theme.iconSizeLarge
        height: Theme.iconSizeLarge
        MouseArea {
            anchors.fill: parent
            onClicked: {
                console.log("Room clicked", room_name)
                pageStack.push(Qt.resolvedUrl("../pages/RoomPage.qml"), { room_id: room_id, room_name: room_name } )
            }
        }
        Image {
            anchors.bottom: itemrec.bottom
            anchors.right: itemrec.right
            source: "image://theme/icon-s-secure"
            visible: encrypted
        }
        SequentialAnimation {
            id: blinkAnim
            running: false
            loops: Animation.Infinite

            NumberAnimation {
                target: itemrec
                property: "opacity"
                duration: 1000
                to: 0.5
            }
            NumberAnimation {
                target: itemrec
                property: "opacity"
                duration: 1000
                to: 1.0
            }
        }

        Label {
            anchors.top: itemrec.top
            anchors.right: itemrec.right
            // text: unread != 0 ? unread : ""
            text: ""
        }
    }


    Connections {
        target: clienthandler
        onStopTyping: {
            blinkAnim.running = false
            itemrec.opacity = 1.0
        }
        onStartTyping: {
            if (room_id !== roomId || user_id === users[0].user_id) {
                return
            }
            blinkAnim.running = true
        }
    }

    Label {
        anchors.horizontalCenter: itemrec.horizontalCenter
        text: room_name
        width: Theme.itemSizeExtraLarge
        height: Theme.itemSizeMedium
        font.pixelSize: Theme.fontSizeTiny
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        padding: 3
    }
}
