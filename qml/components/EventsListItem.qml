import QtQuick 2.0
import Sailfish.Silica 1.0

Grid {
    width: parent.width
    columns: 2
    spacing: 2
    anchors.left: parent.left
    anchors.right: parent.right
    height: eventItem.height > iconColumn.height ? eventItem.height : iconColumn.height

    Column {
        id: iconColumn
        width: 100
        // anchors.left: parent.left
        UserIcon {
            anchors.horizontalCenter: parent.horizontalCenter
            width: Theme.iconSizeMedium
            height: Theme.iconSizeMedium
            uid: user_id
            avatarurl: avatar_url
            idirect: true
            roomname: lname
            visible: lname != ""
        }
        Rectangle {
            width: Theme.iconSizeMedium
            height: Theme.iconSizeMedium
            opacity: 0.0
            visible: lname == ""
        }
    }

    Column {
        id: eventItem
        width: page.width - iconColumn.width
        height: usertime.height + usertime.spacing + msgLabel.height + msgLabel.spacing
        // anchors {
            // left: iconColumn.left
            // right: parent.right
        // }
        Row {
            id: usertime
            width: parent.width
            Label {
                id: userLabel
                horizontalAlignment: Text.AlignLeft
                text: event.type === 'm.sticker' ? "" : (lname + (image !== "" ? qsTr(' has sent an image') : ''))
                color: u_color(user_id)
                font.bold: true
                font.pixelSize: Theme.fontSizeExtraSmall * 1.1
                padding: Theme.paddingMedium
            }
            Label {
                id: dateHour
                width: parent.width - userLabel.width
                horizontalAlignment: Text.AlignRight
                text: {
                    var date = new Date(null)
                    date.setSeconds(edate)
                    return date.toLocaleTimeString().substring(0,5)
                }

                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeTiny
                padding: Theme.paddingMedium
            }

        }
        Image {
            id: imgLabel
            // asynchronous: true
            fillMode: Image.PreserveAspectFit
            visible: image !== ""
            width: parent.width * (event.type === 'm.sticker' ? 0.3 : 1)
            source: image
        }

        Label {
            id: msgLabel
            visible: image === ""

            anchors {
                left: parent.left
                right: parent.right
                margins: Theme.paddingMedium
            }

            text: {
                if (event.type === "m.room.message") {
                    if (event.content.format) {
                        if (event.content.format === 'org.matrix.custom.html') {
                            return bluepill.htmlcss + event.content.formatted_body
                        }
                    }
                    return event.content.body
                } else if (event.type === "m.sticker") {
                    return ""
                } else if (event.type === "m.room.encrypted") {
                    return qsTr("End to end encryption not implemented")
                } else {
                    return "unknown: " + event.type
                }
            }
            wrapMode: Text.WordWrap
            // height: contentHeight
            width: parent.width
            font.pixelSize: Theme.fontSizeExtraSmall * 1.1
            linkColor: Theme.highlightColor
            textFormat: Text.RichText
            onLinkActivated: Qt.openUrlExternally(link)
        }
        Row {
            anchors {
                left: parent.left
                right: parent.right
            }
            height: Theme.iconSizeExtraSmall
        }
    }
}
