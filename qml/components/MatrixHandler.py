#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pyotherside
import threading
import sys
import os
import pyotherside

sys.path.append("/usr/share/harbour-bluepill/python")

def do_login(user, password, homeserver, identserver, hostname):
    """
    log into the matrix server
    """

    
class SystemHandler:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()
        pyotherside.atexit(self.doexit)

    def dologin(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=do_login, args=[user, password,
                                                                homeserver, identserver,
                                                                hostname])
        self.bgthread.start()

    def doexit(self):
        """
        On exit: we need to save ourself
        """

        pass
    
systemhandler = SystemHandler()
