import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: clienthandler

    signal loginFailed()
    signal serverConnected()
    signal needLogin()
    signal loggedIn()
    signal loggedOut()
    signal syncingRooms(string roomname)
    signal connectFailed()
    signal isConnected()
    signal initReady()
    signal hostName(string hostname)
    signal roomsList(var data)
    signal roomData(var room)
    signal roomEvents(var events)
    signal roomMembers(string room_id, var members)
    signal userInfo(var userdata)
    signal mRoomEvent(string roomId, var event)
    signal stopTyping(string roomId)
    signal startTyping(string roomId, var users)
    signal messageSent()

    Component.onCompleted: {
        setHandler("loginFailed", loginFailed)
        setHandler("loggedIn", loggedIn)
        setHandler("syncingRooms", syncingRooms)
        setHandler("serverConnected", serverConnected)
        setHandler("needLogin", needLogin)
        setHandler("connectFailed", connectFailed)
        setHandler("isConnected", isConnected)
        setHandler("initReady", initReady)
        setHandler("hostName", hostName)
        setHandler("roomsList", roomsList)
        setHandler("roomData", roomData)
        setHandler("roomEvents", roomEvents)
        setHandler("roomMembers", roomMembers)
        setHandler("userInfo", userInfo)
        setHandler("mRoomEvent", mRoomEvent)
        setHandler("startTyping", startTyping)
        setHandler("stopTyping", stopTyping)
        setHandler("messageSent", messageSent)
        setHandler("loggedOut", loggedOut)

        addImportPath(Qt.resolvedUrl('.'));
        importModule('ClientHandler', function () {
            console.log('ClientHandler is now imported')
        })
    }

    function initSystem() {
        console.log('system_init')
        call("ClientHandler.do_init", function() {})
    }

    function getHostname() {
        // call("SystemHandler.systemhandler.gethostname", function() {})
        call("ClientHandler.get_hostname", function() {})
    }

    function doLogin(user, password, hostname, homeserver, identserver) {
        // call("ClientHandler.clienthandler.dologin", [user, password, hostname, homeserver, identserver], function() {})
        call("ClientHandler.do_login", [user, password, hostname, homeserver, identserver], function() {})
    }
    function getRooms() {
        call("ClientHandler.clienthandler.getrooms", function() {})
        // call("ClientHandler.get_rooms", function() {})
    }

    function getRoom(room_id) {
        // call("ClientHandler.clienthandler.getroom", [room_id], function() {})
        call("ClientHandler.get_room", [room_id], function() {})
    }

    function getRoomEvents(room_id) {
        // call("ClientHandler.clienthandler.getroomevents", [room_id], function() {})
        call("ClientHandler.get_room_events", [room_id], function() {})
    }

    function getRoomMembers(room_id) {
        call("ClientHandler.clienthandler.getroommembers", [room_id], function() {})
        // call("ClientHandler.get_room_members", [room_id], function() {})
    }

    function getUserInfo() {
        // call("ClientHandler.clienthandler.getuserinfo", function() {})
        call("ClientHandler.get_user_info", function() {})
    }

    function sendMessage(room_id, message) {
        // call("ClientHandler.clienthandler.sendmessage", [room_id, message], function() {})
        call("ClientHandler.send_message", [room_id, message], function() {})
    }

    function doLogout() {
        // call("ClientHandler.clienthandler.dologout", function() {})
        call("ClientHandler.do_logout", function() {})
    }

    function seenMessage(room_id) {
        // call("ClientHandler.clienthandler.sendmessage", [room_id, message], function() {})
        call("ClientHandler.seen_message", [room_id], function() {})
    }

    function isTyping(room_id, timeout) {
        // call("ClientHandler.clienthandler.istyping", [room_id, timeout], function() {})
        call("ClientHandler.is_typing", [room_id, timeout], function() {})
    }

//    function doStart() {
//        console.log('doStart')
//        call("SystemHandler.do_start", function() {})
//    }


//    function getRooms() {
//        call("SystemHandler.systemhandler.getrooms", function() {})
//        // call("SystemHandler.get_rooms", function() {})
//    }




    onError: {
        console.log('python error: ' + traceback);
    }

    onReceived: {
        console.log('got message from python: ' + data);
    }
}
