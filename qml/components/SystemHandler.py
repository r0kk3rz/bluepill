#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pyotherside
import threading
import sys
import os
import pyotherside

sys.path.append("/usr/share/harbour-bluepill/python")

from bluepill.client import BluepillClientFactory
from bluepill.memory import Memory

def get_hostname():
    """
    Return System hostname
    """

    hostname = os.uname().nodename
    pyotherside.send('hostName', hostname)

def do_start():
    """
    just instantiate the client
    """

    while not BluepillClientFactory().get_client():
        pass
    
    pyotherside.send('isStarted')

def system_init(qml_object):
    """
    Initialize the system
    """

    while not BluepillClientFactory().set_qml_object(qml_object):
        pass
    
    while not BluepillClientFactory().get_client():
        pass

    pyotherside.send("After initializing QML object")

def do_login(user, password, hostname, homeserver, identserver):
    """
    login
    """

    token = BluepillClientFactory().login_client_pw(user, password, hostname, homeserver,
                                                    identserver)
    client = BluepillClientFactory().bluepill_client

    if token != "":
        pyotherside.send("loggedIn", token, client.user_id)
    else:
        pyotherside.send("loginFailed")

def do_logout():
    """
    Log out
    """

    BluepillClientFactory().get_client().logout()

def get_rooms():
    """
    Get a list of all rooms
    """

    # get data pickle
    # pyotherside.send("roomsList", data)

    try:
        data = Memory().get_object("roomslist")
        if data:
            pyotherside.send("sending rooms from cache")
            pyotherside.send("roomsList", data)
    except:
        pyotherside.send("roomslist from disk failed")
        pass

    data = []

    client = BluepillClientFactory().get_client()

    pyotherside.send("client: ", client)

    for roominfo in client.get_rooms():
        data.append(roominfo)

    data.sort(key = lambda r: r["time"], reverse=True)
    pyotherside.send("roomsList", data)
    Memory().safe_object("roomslist", data)
    
def get_room_events(room):
    """
    get actual Room events
    """

    try:
        events = Memory().get_object("event-" + room)
        if events:
            pyotherside.send("roomEvents", events)
    except:
        pass
    
    events = []

    try:
        for event in BluepillClientFactory().get_client().get_room_events(room):
            events.append(event)
    
        pyotherside.send("roomEvents", events)
        Memory().safe_object("event-" + room, events)
    except:
        pass
    
def start_room_listener(room):
    """
    start listener for new events
    """

    pyotherside.send("start_room_listener", room)
    BluepillClientFactory().get_client().set_room_listener(room)

def stop_room_listener(room):
    """
    stop the last listener
    """

    pyotherside.send("stop_room_listener", room)
    # BluepillClientFactory().get_client().stop_room_listener(room)

def get_user_info():
    """
    Get info of logged in user
    """

    BluepillClientFactory().get_client().get_user_info()

def send_message(room_id, message):
    """
    Send a message
    """

    BluepillClientFactory().get_client().send_message(room_id, message)
    pyotherside.send("messageSent")

class SystemHandler:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()
        pyotherside.atexit(self.doexit)

    def gethostname(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_hostname)
        self.bgthread.start()

    def dologin(self, user, password, hostname, homeserver, identserver):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.thread(target=do_login, args = [user, password,
                                                                  hostname,
                                                                  homeserver, identserver])
        self.bgthread.start()

    def dologout(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=do_logout)
        self.bgthread.start()

    def getrooms(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_rooms)
        self.bgthread.start()

    def getroomevents(self, room):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_room_events, args=[room])
        self.bgthread.start()

    def startroomlistener(self, room):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=start_room_listener, args=[room])
        self.bgthread.start()

    def stoproomlistener(self, room):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=stop_room_listener, args=[room])
        self.bgthread.start()

    def sendmessage(self, room_id, message):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=send_message, args=[room_id, message])
        self.bgthread.start()
        
    def doexit(self):
        """
        On exit: we need to save ourself
        """

        BluepillClientFactory().get_client().stop_listener()

systemhandler = SystemHandler()
