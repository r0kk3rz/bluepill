"""
Store: a file store for pickling elements
"""

import os
import pickle
import hashlib
from pathlib import Path
import sys
import pyotherside

sys.path.append("../")

class Store:
    def __init__(self, directory):
        """
        initialize the Store.
        directory: File directory the date should be stored to
        """

        self.directory = directory

    def _hashme(self, name):
        """
        create a hash from name
        """

        return hashlib.sha256(name.encode()).hexdigest() + ".pickle"

    def store(self, index, element):
        """
        store the element in self.directory
        index: store index name
        element: the element to be pickeled
        """

        fname = self._hashme(index)
        with open(self.directory + "/" + fname, 'wb') as in_handle:
            pickle.dump(element, in_handle)

    def delete(self, index):
        """
        delete an element file
        index: index of the file
        """

        fname = self._hashme(index)
        path = Path(self.directory + "/" + fname)

        if path.exists():
            os.remove(self.directory + "/" + fname)

    def get(self, index):
        """
        get the element from file
        """


        fname = self._hashme(index)
        path = Path(self.directory + "/" + fname)
        if path.exists():
            pyotherside.send("log", "store get: path found")
            try:
                with open(self.directory + "/" + fname, 'rb') as out_handle:
                    return pickle.load(out_handle)
            except:
                return None
        else:
            return None

    def exists(self, index):
        """
        check if an index exists as persitent file
        index: The file index
        """

        fname = self._hashme(index)
        path = Path(self.directory + "/" + fname)
        return path.exists()
