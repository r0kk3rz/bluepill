"""
Memory factory
"""

PROGNAME = 'harbour-bluepill'

import sys
import os
import pyotherside

sys.path.append("../")

from bluepill.singleton import Singleton
from bluepill.cache import Cache
from bluepill.store import Store
from bluepill import util

class Memory(metaclass=Singleton):
    """
    Memory class for caching and storing data to disk
    """

    def __init__(self, progname = PROGNAME):
        """
        Initialization
        """

        self._objcache = Cache(limit=50)

        home = os.path.expanduser('~')
        xdg_cache_home = os.path.join('XDG_CACHE_HOME', os.path.join(home, '.cache'))

        self._cache_home = os.path.join(xdg_cache_home, progname)

        util.make_directory(self._cache_home)

        self._store = Store(self._cache_home)

    @property
    def cache_home(self):
        return self._cache_home

    def get_object(self, index):
        """
        Get an object
        """

        pyotherside.send("get_object", index)

        obj = self._objcache.get(index)
        if not obj:
            obj = self._store.get(index)
            if not obj:
                return None
            else:
                self._objcache.store(index, obj)

        return obj

    def save_object(self, index, obj):
        """
        Save object to disk
        """

        pyotherside.send("log", "save_object", index)
        self._store.store(index, obj)

        pyotherside.send("cache_object", index)
        self._objcache.store(index, obj)

    def delete_object(self, index):
        """
        Delete object from disk
        """

        self._store.delete(index)
        self._objcache.delete(index)
