"""
some utilities
"""

import os
import urllib.request
import pyotherside

import time
import datetime

user_agent = 'PodQast/2.3 +https://gitlab.com/cy8aer/podqast'
user_agent2 = 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'

def make_directory(path):
    """
    Tries to create a directory if it does not exist already
    Return: True if directory exists, False otherwise
    """

    if os.path.isdir(path):
        return True

    try:
        os.makedirs(path, 0o755)
    except:
        return False

    return True

def make_symlink(frompath, topath):
    """
    create a symlink
    """

    if os.path.islink(topath):
        return True

    try:
        os.symlink(frompath, topath)
    except:
        return False

    return True

def dl_from_url(url, path):
    """
    Download a file from url and save to path
    """

    agent = user_agent

    rsp = urllib.request.Request(url, data=None, headers = {
        'User-Agent' : agent
        })
    response = urllib.request.urlopen(rsp)
    with open(path, 'wb') as fhandle:
        fhandle.write(response.read())

def delete_file(path):
    """
    Delete file if exists
    """
    if os.path.exists(path):
        os.remove(path)

def format_date(timestamp):
    """
    Convert a UNIX timestamp to a date representation. From gpodder
    """

    if timestamp is None:
        return None

    seconds_in_a_day = 60 * 60 * 24

    try:
        timestamp_date = time.localtime(timestamp)[:3]
    except ValueError as ve:
        return None

    try:
        diff = int((time.time() - timestamp) / seconds_in_a_day)
    except:
        return None

    try:
        timestamp = datetime.datetime.fromtimestamp(timestamp)
    except:
        return None

    if diff < 7:
        return timestamp.strftime('%A')
    else:
        return timestamp.strftime('%x')

def format_full_date(timestamp):
    """
    Get a full date string (monday..., may 2018, 2017, ...)
    """

    now = datetime.datetime.now()
    tnow = time.time()
    diff = tnow - timestamp
    if diff < 60 * 60 * 24 * 7:
        return format_date(timestamp)

    dt = datetime.datetime.fromtimestamp(timestamp)
    if dt.year == now.year:
        return dt.strftime('%B %Y')
    return dt.strftime('%Y')
