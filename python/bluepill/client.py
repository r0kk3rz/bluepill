"""
Bluepill client functionality
"""

STORE_NAME = "BluepillClient"

import pyotherside
import sys
import os
from urllib.parse import urlparse
import re

sys.path.append("../")

from matrix_client.client import MatrixClient
from matrix_client.errors import *

from bluepill.singleton import Singleton
from bluepill.client_data import ClientDataFactory
from bluepill.client_data import CLIENTDATA
from bluepill.room_data import RoomDataFactory
from bluepill import util
from bluepill.memory import Memory
from markdown2 import Markdown

class BluepillClient():
    """
    Bluepill Client
    """

    def __init__(self):
        """
        Initialization of the client
        """

        # Connectivity data

        self._data = ClientDataFactory().clientData

        # Matrix connector data

        self._client = None               # the Matrix Client
        self._matrix = None               # Matrix API
        # self._room_listener_uid = None    # Room listener
        self._m_room_message_listener = None
        self._m_room_sticker_listener = None
        self._m_typing_listener = None

        link_patterns=[(re.compile(r'((([@!#]?[A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+(:[0-9]+)?|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)'),r'\1')]
        self._markdown = Markdown(extras=["link-patterns"],link_patterns=link_patterns)

    def logout(self):
        """
        Log system out
        """

        self._client.logout()
        self._client = None
        ClientDataFactory().client_data = None
        Memory().delete_object(CLIENTDATA)
        self._data = ClientDataFactory().clientData
        pyotherside.send("loggedOut")

    def login(self, user, password, hostname, homeserver, identserver):
        """
        Login to the server. Will initialize token
        """

        pyotherside.send("login")
        try:
            self._client = MatrixClient(homeserver)
            self._data.token = self._client.login(user, password, limit=100, sync=True)
            pyotherside.send("gotToken", self._data.token)
        except MatrixRequestError:
            pyotherside.send("loginFailed")
            return

        pyotherside.send("loggedIn")

        self._data.homeserver = homeserver
        self._data.identserver = identserver
        self._data.user_id = self._client.user_id
        self._data.device_id = self._client.device_id
        self._data.encryption = True
        user = self._client.get_user(self._data.user_id)
        self._data.user_name = user.get_display_name()
        avatar_url = user.get_avatar_url()
        if not avatar_url:
            self._data.user_avatar_url = ""
        else:
            self._data.user_avatar_url = self._get_url(avatar_url)

        response = self._client.api.sync()

        self.get_room_dir(response)


        self.get_rooms()

        self._data.save()
        self.start_room_event_listener()
        self.start_listener()
        pyotherside.send("initReady")
        pyotherside.send("log", "token", self._data.token)

    def connect(self):
        """
        Connect to the matrix server. If no token exists, signal needLogin
        """

        if not self._data.token:
            pyotherside.send("needLogin")
            return

        pyotherside.send("log", "token", self._data.token)
        try:
            # enc_conf = { "device_id": self._data.device_id}
            self._client = MatrixClient(self._data.homeserver,
                                        user_id = self._data.user_id,
                                        token = self._data.token)

            #                            encryption = self._data.encryption,
            #                            encryption_conf = enc_conf)
        except:
            pyotherside.send("connectFailed")
            self._client = None
            pyotherside.send("initReady")
            return

        pyotherside.send("isConnected")

        self.sync_rooms()
        self.start_room_event_listener()
        self.start_listener()
        pyotherside.send("initReady")

    def do_exit():
        """
        Stop the listener Threads
        """

        self.stop_listener()
        self.stop_room_event_listener()

    def get_room_dir(self, response):
        """
        Get direct rooms from account_data
        """

        account_data = response["account_data"]
        for event in account_data["events"]:
            if event["type"] == 'm.direct':
                self._data.roomdir = []
                directs = event["content"]
                for i in directs.values():
                    for j in i:
                        self._data.roomdir.append(j)

    def room_append_event(self, room_id, event, save=True):
        """
        Append a room event
        """

        room_data = RoomDataFactory().room_data(room_id)
        if not room_data:
            return None
        image = ""
        user_id = event["sender"]
        user = self._client.get_user(user_id)
        try:
            avatar_url = user.get_avatar_url()
        except:
            avatar_url = ""

        if avatar_url:
            avatar_url = self._get_url(avatar_url)
        else:
            avatar_url = ""
        dat = event["origin_server_ts"] / 1000
        event["origin_server_ts"] = dat
        try:
            name = user.get_friendly_name()
        except:
            name = ""
        try:
            if event["content"]["msgtype"] == 'm.image':
                image = event["content"]["url"]
                pyotherside.send("image: ", image)
                if image:
                    image = self._get_url(self._client.api.get_download_url(image))
                else:
                    image = ""
        except:
            image = ""

        try:
            if event["type"] == 'm.sticker':
                image = event["content"]["url"]
                if image:
                    image = self._get_url(self._client.api.get_download_url(image))
        except:
            image = ""

        try:
            if event["content"]["msgtype"] == 'm.text':
                event["content"]["formatted_body"] = self._markdown.convert(event["content"]["body"])
                event["content"]["format"] = "org.matrix.custom.html"
        except:
            pass

        asect = util.format_full_date(dat)

        event_data = {
            'event': event,
            'name': name,
            'user_id': user_id,
            'edate': dat,
            'avatar_url': avatar_url,
            'image': image,
            'asect': asect
            }
        room_data.events.append(event_data)
        room_data.last_time = dat
        if save:
            room_data.end_token = self._client.api.get_room_messages(room_id,
                                                                     None, "f")["end"]
            room_data.save()
        return event_data

    def _create_room(self, room):
        """
        Create a room
        """

        room_data = RoomDataFactory().room_data(room)
        rd = self._client.rooms[room]
        room_data.room_id = room
        room_data.name = rd.name
        pyotherside.send("syncingRooms", rd.display_name)
        room_data.display_name = rd.display_name
        room_data.topic = rd.topic
        tags = rd.get_tags()["tags"]
        for tag in tags:
            if tag == 'm.favourite':
                room_data.level = 'fav'
                if 'order' in tags[tag]:
                    room_data.lorder = tags[tag]["order"]
                else:
                    room_data.lorder = 0
            elif tag == 'm.lowpriority':
                room_data.level = "low"
            else:
                room_data.level = "normal"
        events = rd.get_events()
        room_data.events = []
        for event in events:
            self.room_append_event(room, event, save=False)

        if room in self._data.roomdir:
            for member in rd.get_joined_members():
                if member.user_id != self._data.user_id:
                    try:
                        avatar_url = member.get_avatar_url()
                        user_id = member.user_id
                        if avatar_url:
                            room_data.avatar_url = self._get_url(avatar_url)
                    except:
                        avatar_url = ""
                        user_id = ""
        else:
            for e in self._client.api.get_room_state(room):
                if e["type"] == 'm.room.avatar':
                    aurl = e["content"]["url"]
                    if aurl:
                        avatar_url = self._client.api.get_download_url(aurl)
                        room_data.avatar_url = self._get_url(avatar_url)
            user_id = room

        room_data.user_id = user_id
        room_data.encrypted = rd.encrypted
        room_data.end_token = self._client.api.get_room_messages(room,
                                                                 None, "f")["end"]
        room_data.start_token = self._client.api.get_room_messages(room,
                                                                   None, "f")["start"]
        room_data.members = self.get_room_members(room)

        room_data.save()

    def _sync_room(self, room):
        """
        Sync room data
        """

        rd = self._client.rooms[room]
        room_data = RoomDataFactory().room_data(room)
        if not room_data.room_id:
            self._create_room(room)
            return

        room_data.name = rd.name
        room_data.display_name = rd.display_name
        room_data.topic = rd.topic
        tags = rd.get_tags()["tags"]
        for tag in tags:
            if tag == 'm.favourite':
                room_data.level = 'fav'
                room_data.lorder = tags[tag]["order"]
            elif tag == 'm.lowpriority':
                room_data.level = "low"
            else:
                room_data.level = "normal"
        events = self._client.api.get_room_messages(room,
                                                    room_data.end_token, "f")["chunk"]
        for event in events:
            pyotherside.send("newEvent")
            self.room_append_event(room, event, save=False)

        room_data.end_token = self._client.api.get_room_messages(room,
                                                                     None, "f")["end"]
        room_data.save()

    def sync_rooms(self):
        """
        Synchronize rooms from last time
        """

        self._data.rooms = []
        for room in self._client.rooms:
            self._sync_room(room)
            self._data.rooms.append(room)

        pyotherside.send("initReady")

    def get_rooms(self):
        """
        Initial room fetching and preparing room_data objects
        """

        for room in self._client.rooms:
            self._create_room(room)
            self._data.rooms.append(room)

    def get_roomlist(self):
        """
        Get roomlisst
        """

        for room in self._data.rooms:
            rd = Memory().get_object(room)
            pyotherside.send("log", "outputting", rd.room_id)
            if rd:
                yield {
                    'room_id': rd.room_id,
                    'user_id': rd.user_id,
                    'room_name': rd.display_name,
                    'prio': rd.level,
                    'direct': room in self._data.roomdir,
                    'avatar_url': rd.avatar_url,
                    'time': rd.last_time,
                    'encrypted': rd.encrypted
                    }

    def get_room(self, room_id):
        """
        get single room data
        """
        rd = Memory().get_object(room_id)
        if rd:
            return {
                    'room_id': rd.room_id,
                    'user_id': rd.user_id,
                    'room_name': rd.display_name,
                    'prio': rd.level,
                    'direct': room_id in self._data.roomdir,
                    'avatar_url': rd.avatar_url,
                    'time': rd.last_time,
                    'encrypted': rd.encrypted}

        return None

    def listener_exception(self, e):
        """
        Listener exception
        """

        pyotherside.send("Listener exception")

    def seen_message(self, room_id):
        """
        Send message receipt
        """

        rd = RoomDataFactory().room_data(room_id)

        if not rd:
            return

        levent = rd.events[len(rd.events) - 1]["event"]

        try:
            lr = rd.last_read
        except:
            lr = 0
        if lr < levent["origin_server_ts"]:
            pyotherside.send("seen_message")
            rd.last_read = levent["origin_server_ts"]
            self._client.api.send_read_markers(room_id,
                                               levent["event_id"], levent["event_id"])
            rd.save()

    def is_typing(self, room_id, timeout=30000):
        """
        Send typing
        """

        self._client.rooms[room_id].send_typing(timeout)

    def start_listener(self):
        """
        start listener_thread
        """

        try:
             self._client.start_listener_thread(exception_handler = self.listener_exception)
        except:
            pyotherside.send("Starting listener thread failed")

    def stop_listener(self):
        """
        stop the listener_thread
        """

        try:
            self.client._stop_listener_thread()
        except:
            pyotherside.send("Stopping listener thread failed")

    def room_listener(self, event):
        """
        The room listener
        """

        room_id = event["room_id"]
        event_data = self.room_append_event(room_id, event)
        pyotherside.send("mRoomEvent", room_id, event_data)

    def typing_listener(self, event):
        """
        A user is typing (or not)
        """

        if len(event["content"]["user_ids"]) == 0:
            pyotherside.send("stopTyping", event["room_id"])
            return

        users = []

        for user_id in event["content"]["user_ids"]:
            user = self._client.get_user(user_id)
            user_name = user.get_display_name()
            avatar_url = user.get_avatar_url()
            if not avatar_url:
                image = ""
            else:
                image = self._get_url(avatar_url)


            users.append({
                "user_id": user_id,
                "user_name": user_name,
                "image": image
                })
        pyotherside.send("startTyping", event["room_id"], users)

    def stop_room_event_listener(self):
        """
        Leave listener
        """

        self._client.remove_listener(self._m_room_message_listener)
        self._client.remove_listener(self._m_room_sticker_listener)
        self._client.remove_ephemeral_listener(self._m_typing_listener)
        self._m_room_message_listener = None
        self._m_room_sticker_listener = None
        self._m_typing_listener = None

    def start_room_event_listener(self):
        """
        set room listener
        """

        if self._m_room_message_listener:
            self._client.remove_listener(self._m_room_message_listener)
        if self._m_room_sticker_listener:
            self._client.remove_listener(self._m_room_sticker_listener)

        self._m_room_message_listener = self._client.add_listener(self.room_listener,
                                                            event_type = "m.room.message")
        self._m_room_sticker_listener = self._client.add_listener(self.room_listener,
                                                                event_type = "m.sticker")
        self._m_typing_listener = self._client.add_ephemeral_listener(self.typing_listener,
                event_type = "m.typing")

    def get_room_members(self, room_id):
        """
        get members and event list for room_id
        """

        members = []
        for member in self._client.api.get_room_members(room_id)['chunk']:
            try:
                if member['content']['avatar_url']:
                    image = self._get_url(member['content']['avatar_url'])
                else:
                    image = ""
            except:
                image = ""

            try:
                members.append({
                    'event_id': member['event_id'],
                    'name': member['content']['displayname'],
                    'avatar_url': image,
                    'time': member['origin_server_ts'] / 1000
                })
            except:
                pass

        return members

    def get_room_events(self, room_id):
        """
        get room events for room_id
        """

        rd = Memory().get_object(room_id)
        events = rd.get_room_events()
        members = rd.members
        pyotherside.send("roomEvents", events, members)

    def room_member_list(self, room_id):
        """
        Send room member list
        """
        rd = Memory().get_object(room_id)
        pyotherside.send("roomMembers", rd.members)

    def get_user_info(self):
        pyotherside.send("userInfo", {
            'lname': self._data.user_name,
            'user_id': self._data.user_id,
            'avatar_url': self._data.user_avatar_url
            })

    def _get_url(self, url):
        """
        """

        if url == "":
            return url

        f = urlparse(url).path
        filename = f[f.rfind('/') + 1:]
        path = os.path.join(Memory().cache_home, filename)
        if not os.path.exists(path):
            pyotherside.send("get_url: need to download", path)
            try:
                util.dl_from_url(url, path)
            except:
                return ""

        return path


    def sync(self, first = False):
        """
        Sync with server
        """

        try:
            syncdata = matrix.sync(since = self._ts_token, full_state = first)
        except:
            syncdata = None

        if not syncdata:
            pyotherside.send("Matrix syncing failed")
            return

        for event in syncdata["account_data"]["events"]:
            if event["type"] == 'm.direct':
                self._data.roomdir = []
                directs = event["content"]
                for i in self.directs.values():
                    for j in i:
                        self._data.roomdir.append(j)

            if event["type"] == 'com.gitlab.cy8aer.bluepill':
                # Server saved configuration for my bluepill clients
                pass

        for room in syncdata["rooms"]["join"]:
            # configuration of rooms
            pass

        for room in syncdata["rooms"]["invite"]:
            # you are invited to new rooms
            pass

        for room in syncdata["rooms"]["leave"]:
            # you left rooms or you have been banned from a room
            pass

        for presence in syncdata["presence"]["events"]:
            # presence of users
            # content:
            #     currently_active
            #     last_active_ago
            #     presence
            # sender
            # type
            pass

        # device_lists

        # device_time_keys_count

        self._ts = syncdata["next_batch"]

    def do_exit(self):
        """
        Exit the system
        """

        self.save()

    def save(self):
        """
        Save myself
        """

        # Memory().save_object(STORE_NAME, self)

    # Data providers

    def send_message(self, room_id, message):
        """
        Markdown transform a message and send it
        """

        pyotherside.send("send_message", room_id, message)
        htmlmsg = self._markdown.convert(message)
        room = self._client.rooms[room_id]
        pyotherside.send("sending message ", htmlmsg, message)
        room.send_html(htmlmsg, body=message)

    @property
    def user_name(self):
        """
        Return the user_name
        """

        return self._user_name

    @property
    def user_id(self):
        """
        Return the user_id
        """

        return self._user_id

class BluepillClientFactory(metaclass=Singleton):
    """
    Factory which creates a BluepillClient if it does not exist
    """

    def __init__(self):
        """
        Initialization
        """

        self._bluepill_client = None

    def get_client(self):
        """
        get the Bluepill Client
        """

        if not self._bluepill_client:
            self._bluepill_client = BluepillClient()

        return self._bluepill_client
