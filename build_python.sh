#!/bin/bash
# This is done via coderus' docker container
# in bluepill root directory just call:
# ./build_python.py

docker run -v $(pwd)/build:/home/nemo/build --rm --name=sdk -ti \
    coderus/sailfishos-platform-sdk:3.0.2.8 /home/nemo/build/build.sh

# cleanup is missing!

