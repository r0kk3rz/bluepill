<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>CoverPage</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>is typing...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DashboardPage</name>
    <message>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Dashboard</source>
        <translation>Übersicht</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation>Favoriten</translation>
    </message>
    <message>
        <source>Persons</source>
        <translation>Personen</translation>
    </message>
    <message>
        <source>Rooms</source>
        <translation>Räume</translation>
    </message>
    <message>
        <source>Low priority</source>
        <translation>Niedrige Priorität</translation>
    </message>
</context>
<context>
    <name>EventsListItem</name>
    <message>
        <source>End to end encryption not implemented</source>
        <translation>Ende zu Ende Verschlüsselung is nicht implementiert</translation>
    </message>
    <message>
        <source> has sent an image</source>
        <translation> hat ein Bild geschickt</translation>
    </message>
</context>
<context>
    <name>LogStatusPage</name>
    <message>
        <source>Starting engine...</source>
        <translation type="unfinished">Starte Funktionseinheit...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login</source>
        <translation>Anmelden</translation>
    </message>
    <message>
        <source>Register</source>
        <translation>Registrieren</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Username</translation>
    </message>
    <message>
        <source>Custom Server</source>
        <translation>Benutzerdefinierter Server</translation>
    </message>
    <message>
        <source>https://matrix.org</source>
        <translation>https://matrix.org</translation>
    </message>
    <message>
        <source>https://vector.im</source>
        <translation>https://vector.im</translation>
    </message>
    <message>
        <source>Home server URL</source>
        <translation>Heimserver-URL</translation>
    </message>
    <message>
        <source>Identity server URL</source>
        <translation>Identitätsserver-URL</translation>
    </message>
    <message>
        <source>Client Name</source>
        <translation>Gerätename</translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Abmelden</translation>
    </message>
    <message>
        <source>Logging out</source>
        <translation>Melde mich ab</translation>
    </message>
</context>
<context>
    <name>RegisterPage</name>
    <message>
        <source>Register</source>
        <translation>Registrieren</translation>
    </message>
    <message>
        <source>Email address (optional)</source>
        <translation>E-Mail-Adresse (Optional)</translation>
    </message>
</context>
<context>
    <name>RoomPage</name>
    <message>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Rendering</source>
        <translation>Zeichne</translation>
    </message>
    <message>
        <source>Collecting Posts</source>
        <translation>Sammle Sendungen</translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <source>Starting engine...</source>
        <translation>Starte Funktionseinheit...</translation>
    </message>
    <message>
        <source>Connected...</source>
        <translation>Verbunden...</translation>
    </message>
    <message>
        <source>Not connected, trying later...</source>
        <translation>Nicht verbunden, versuche es später...</translation>
    </message>
</context>
<context>
    <name>TextControl</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source> ist typing...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
