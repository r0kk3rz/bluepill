<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="21"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="80"/>
        <source>is typing...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DashboardPage</name>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="19"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="68"/>
        <source>Dashboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="90"/>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="125"/>
        <source>Persons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="160"/>
        <source>Rooms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="195"/>
        <source>Low priority</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EventsListItem</name>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="48"/>
        <source> has sent an image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="100"/>
        <source>End to end encryption not implemented</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LogStatusPage</name>
    <message>
        <location filename="../qml/pages/LogStatusPage.qml" line="41"/>
        <source>Starting engine...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="54"/>
        <location filename="../qml/pages/LoginPage.qml" line="141"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="76"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="93"/>
        <source>Client Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="100"/>
        <source>Custom Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="105"/>
        <source>Home server URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="115"/>
        <source>https://matrix.org</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="121"/>
        <source>Identity server URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="131"/>
        <source>https://vector.im</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="178"/>
        <source>Register</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="26"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="47"/>
        <source>Logout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="55"/>
        <source>Logging out</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegisterPage</name>
    <message>
        <location filename="../qml/pages/RegisterPage.qml" line="21"/>
        <location filename="../qml/pages/RegisterPage.qml" line="48"/>
        <source>Register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RegisterPage.qml" line="42"/>
        <source>Email address (optional)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RoomPage</name>
    <message>
        <location filename="../qml/pages/RoomPage.qml" line="49"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomPage.qml" line="117"/>
        <source>Rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomPage.qml" line="118"/>
        <source>Collecting Posts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <location filename="../qml/pages/StartPage.qml" line="21"/>
        <source>Connected...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/StartPage.qml" line="24"/>
        <source>Not connected, trying later...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/StartPage.qml" line="53"/>
        <source>Starting engine...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextControl</name>
    <message>
        <location filename="../qml/components/TextControl.qml" line="57"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/components/TextControl.qml" line="107"/>
        <source> ist typing...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
